var animationSpeed = 100;
var canMove=false;

var LEFT_KEY = 37;
var RIGHT_KEY = 39;
var UP_KEY = 38;
var DOWN_KEY = 40;

function moveTiles(direction, operator, length, thisElement, col, row, to)
{
    if(canRemove(thisElement))
    {
        if(direction == "horizontal")
        {
            setMergedTileValue('.tile[data-col=' + to + '][data-row=' + row + ']', parseInt($(thisElement).html() * 2));
        }
        else
        {
            setMergedTileValue('.tile[data-col=' + col + '][data-row=' + to + ']', parseInt($(thisElement).html() * 2));
        }
        $(thisElement).remove();
    }

    $('.boardtile[data-col='+col+'][data-row='+row+']').removeClass('filled').addClass('empty');

    if(direction == "horizontal")
    {
        $(thisElement).animate({left: operator+length},animationSpeed);
        
        $(thisElement).attr('data-col',to);
        $('.boardtile[data-col='+to+'][data-row='+row+']').removeClass('empty').addClass('filled');
    }
    else
    {
        $(thisElement).animate({top: operator+length},animationSpeed);
        $(thisElement).attr('data-row',to);
        $('.boardtile[data-col='+col+'][data-row='+to+']').removeClass('empty').addClass('filled');  
    }
}

function markAsRemoval(element)
{
    $(element).attr('data-remove',1);
}

function clearRemovalMark(element)
{
    $(element).attr('data-remove',0);
}

function getColumn(element)
{
    return parseInt($(element).attr('data-col'));
}

function getRow(element)
{
    return parseInt($(element).attr('data-row'));
}

function isFilled(col, row)
{
    return $('.boardtile[data-col='+col+'][data-row='+row+']').hasClass('filled');
}

function isEqualWithAdjacentTile(thisElement, col, row)
{
    return $(thisElement).html() ==  $('.tile[data-col='+col+'][data-row='+row+']').html();
}

function canRemove(element)
{
    return $(element).attr('data-remove')==1;
}

function addNewTile() 
{
    var emptys = $('.empty').length;
    var randomPos = Math.floor(Math.random() * emptys);
    var tile = $('.empty').eq(randomPos);
    $(tile).removeClass('empty');
    $(tile).addClass('filled');
    var tilePosition = $(tile).position();

    var randIndex = Math.floor((Math.random() * 10) + 1); 
    var newRandomValue = 2;

    if(randIndex>5)
    {
        newRandomValue = 4;
    }


    $('#boardcontainer').append('<div id = "newesttile" class = "tile tile-'+newRandomValue+'" data-row="' + $(tile).attr('data-row') + '" data-col="' + $(tile).attr('data-col') + '">'+newRandomValue+'</div>')
    $('#newesttile').css({
        top: (tilePosition.top + 8) + 'px',
        left: (tilePosition.left + 8) + 'px'
    })
    $('#newesttile').fadeTo(animationSpeed * 3, 1);
    $('#newesttile').attr('id', '');
    canMove = true;
}

function setMergedTileValue(selector, value)
{
    resetTileClass(selector);
    $(selector).html(value);
    $(selector).addClass('tile-' + value);
    updateScore(value);
}

function resetTileClass(selector)
{
    for(i = 2; i<=4049; i*=2)
    {
        $(selector).removeClass('tile-' + i);
    }
}

function updateScore(newAdditionalScore) 
{
    var score = parseInt($('#score').html());
    score+=newAdditionalScore;
    $('#score').html(score);
}

function resetGame()
{
    $(".tile").each(function()
    {
        var images = $(this).remove();
    });

    $(".boardtile").each(function()
    {
        if($(this).hasClass('filled'))
        {
            $(this).removeClass('filled');
            $(this).addClass('empty');
        }
    });

    startNewGame();
}

function startNewGame()
{
    addNewTile();
    addNewTile();
}

function moveDown()
{
    var isAnyTileMoved = false;
    $('.tile').sort(function(a,b)
    {
        var rowA = getRow(a);
        var rowB = getRow(b);
        return rowB - rowA;
    }).
    each(function()
    {
        var col = getColumn(this);
        var row = getRow(this);
        var to = row;

        clearRemovalMark(this);

        if(row<4)
        {
            for(i=row+1;i<=3;i++)
            {
                if(isFilled(col, i))
                {
                    if(isEqualWithAdjacentTile(this, col, i))
                    {
                        markAsRemoval(this);
                        to = i;
                    }
                    break;
                }
                else
                {
                    to = i;
                }
            }
        
            isAnyTileMoved= to!=row;
            var movementLength = (116*(to-row));
            moveTiles("vertical", '+=', movementLength, this, col, row, to);
 
        }
    });
    return isAnyTileMoved;
}

function moveUp()
{
    var isAnyTileMoved = false;
    $('.tile').sort(function(a,b)
    {
        var rowA = getRow(a);
        var rowB = getRow(b);
        return rowA - rowB;
    }).each(function()
    {
        var col = getColumn(this);
        var row = getRow(this);
        var to = row;

        clearRemovalMark(this);
        
        if(row>0)
        {
            for(i=row-1;i>=0;i--)
            {
                if(isFilled(col, i))
                {
                    if(isEqualWithAdjacentTile(this, col, i))
                    {
                        markAsRemoval(this);
                        to = i;
                    }
                    break;
                }
                else
                {
                    to = i;
                }
            }

            isAnyTileMoved= to!=row;
            var movementLength = (116*(row-to));
            moveTiles("vertical", '-=', movementLength, this, col, row, to);
        }
    });
    return isAnyTileMoved;
}

function moveRight()
{
    var isAnyTileMoved = false;

    $('.tile').sort(function(a,b)
    {
        var colA = getColumn(a);
        var colB = getColumn(b);
        return colB - colA;
    }).each(function()
    {
        var col = getColumn(this);
        var row = getRow(this);
        var to = col;

        clearRemovalMark(this);

        if(col<4)
        {
            for(i=col+1;i<=3;i++)
            {
                if( isFilled(i, row))
                {
                    if(isEqualWithAdjacentTile(this, i, row))
                    {
                        markAsRemoval(this);
                        to = i;
                    }
                    break;
                }
                else
                {
                    to = i;
                }
            }

            isAnyTileMoved = (col!=to);
            var movementLength = 116*(to-col);
            moveTiles("horizontal", '+=', movementLength, this, col, row, to);
        }
    });
    return isAnyTileMoved;
}

function moveLeft()
{
    var isAnyTileMoved = false;

    $('.tile').sort(function(a,b)
    {
        var colA = getColumn(a);
        var colB = getColumn(b);
        return colA - colB;
    }).each(function()
    {
        var col = getColumn(this);
        var row = getRow(this);
        var to = col;

        clearRemovalMark(this);

        if(col>0)
        {
            for(i=col-1;i>=0;i--)
            {
                if(isFilled(i, row))
                {
                    if(isEqualWithAdjacentTile(this, i, row))
                    {
                        markAsRemoval(this);
                        to = i;
                    }
                    break;
                }
                else
                {
                    to = i;
                }
            }

            isAnyTileMoved = (col!=to);
            var movementLength = 116*(col-to);
            moveTiles("horizontal", '-=', movementLength, this, col, row, to);
        }
    });
    return isAnyTileMoved;
}

