$(document).ready(function()
{
    startNewGame();

    $( ".newgame > a" ).click(function() 
    {
        resetGame();
    });
    
});


$(document).keydown(function(event)
{
    if ($(event.target).is('input, textarea')) 
    {
        return;
    }

    if(canMove)
    {
        canMove=false; 
        isAnyTileMoved=false;

        switch(event.keyCode)
        {
            case LEFT_KEY:
                isAnyTileMoved = moveLeft();
            break;
            
            case RIGHT_KEY:
                isAnyTileMoved = moveRight();
            break;
            
            case UP_KEY:
                isAnyTileMoved = moveUp();
            break;
            
            case DOWN_KEY:
                isAnyTileMoved = moveDown();
            break;
        }
                
        if(isAnyTileMoved)
        {
            addNewTile();
        }
        else
        {
            canMove=true;
        }
    }
});